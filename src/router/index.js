import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
export default new Router({

	routes: [
		{
			path: '/login',
			component: () => import("@/views/login.vue"),
			name: "login",
			meta: {
				noBack: true
			},
		},
		{
			path: '/',
			redirect: 'index'
		},
		{
			path: '/index',
			component: () => import("@/views/index.vue"),
			name: "首页",
			meta: {
				requiresAuth: true
			},
			children: [{
					path: '/Humanities',
					component: () => import("@/components/Humanities/index.vue"),
					name: "人文数据",
					meta: { requiresAuth: true },
				},
				{
					path: '/Assess',
					component: () => import("@/components/Assess/index.vue"),
					name: "评估与区划",
					meta: { requiresAuth: true },
				},
				{
					path: '/Model',
					component: () => import("@/components/Model/index.vue"),
					name: "模型",
					meta: { requiresAuth: true },
				},
				{
					path: '/Nature',
					component: () => import("@/components/Nature/index.vue"),
					name: "自然数据",
					meta: { requiresAuth: true },
				},
				{
					path: '/User',
					component: () => import("@/components/User/index.vue"),
					name: "系统设置",
					meta: { requiresAuth: true },
				},
			]
		},

	],

})

