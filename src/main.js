import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'

Vue.use(Router)
import router from './router'
import '@/assets/global.less' /*引入公共样式*/

import Axios from 'axios'
import qs from 'qs'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import './components/index'
import * as echarts from 'echarts'
import 'leaflet/dist/leaflet.css';
import global from './config/leafletConf'
import store from './store'
import 'backbone';
import 'underscore'
import jQuery from 'jquery';
window.$ = window.jQuery = jQuery;
import 'topojson'

// 将Decimal挂载到Vue的原型上
//局部或全局定义
Vue.prototype.$echarts = echarts
Vue.prototype.back = function () {
    if (window.history.length <= 1) {
        this.$router.push({
            path: '/'
        })
        return false
    } else {
        this.$router.go(-1)
    }
}

Vue.prototype.openUrl = function (url) {
    this.$router.push({
        path: url
    })

}

Vue.prototype.random = function (min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}


const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}


Vue.prototype.$axios = Axios
Vue.prototype.HOST = 'http://192.168.3.66:8000/testhuanengapi/miracast/'
// Vue.prototype.HOST = 'http://47.96.153.134:443/miracast/'
//Vue.prototype.HOST = 'http://192.168.3.66:8080/'
// Vue.prototype.HOST = 'http://127.0.0.1:5368/miracast/'
http: //192.168.3.66:8080/readResourceFileByFileName.json
    //Axios.defaults.baseURL = 'http://api.douban.com/v2'
    Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

// 添加请求拦截器
Axios.interceptors.request.use(function (config) {
    if (config.method === 'post') {
        config.data = qs.stringify(config.data)
    }
    return config
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
})

// 添加响应拦截器
Axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    return response
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error)
})

Vue.use(ElementUI);
Vue.prototype.$GlobalMap = global

Vue.config.productionTip = false
// 路由解析守卫，从首页跳转后，后退不回到登录页
router.beforeResolve((to, from, next) => {
    if (to.meta.noBack && from.name === '首页') {
        // 如果目标路由有noBack标志，并且是从首页过来的，重定向到首页并清除历史记录
        window.history.go(-1);
        setTimeout(() => {
            window.history.go(1);
            next({
                name: '首页',
                replace: true
            });
        }, 0);
    } else {
        next();
    }
});
// 设置全局前置导航守卫
router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('user'); // 假设你将token存储在localStorage中

    if (to.meta.requiresAuth) {
        // 如果目标路由有requiresAuth标志
        if (token) {
            // 如果存在token，则允许访问并继续导航
            next();
        } else {
            // 如果不存在token，则重定向到登录页
            next('/login');
        }
    } else {
        // 如果目标路由不需要身份验证，则直接继续导航
        next();
    }
});
router.beforeEach((to, from, next) => {
	if (to.meta.requiresAuth && !localStorage.getItem('user')) {
	  next({ name: 'login' }); // 如果需要登录但未登录，重定向到登录页
	} else {
	  next(); // 允许访问
	}
  });

new Vue({
    render: h => h(App),
    router,
    store,
}).$mount('#app')
