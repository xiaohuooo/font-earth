import api from '@/common/request';


const dataTypeOption = [
    {
        name: '海温', //
        value: 'seaTemperature',
        startTime: 1979,
        endTime: 2023,
        range: {
            min: '11.7',
            max: '31.4',
            step: '0.1',
            logotype: '°C'
        },
    },
    {
        name: '高于大地水准的海面高度',
        value: 'theHeightOfTheSeaAboveTheGeoid',
        startTime: 1989,
        endTime: 2022,
        range: {
            min: '0.1',
            max: '1.2',
            step: '0.1',
            logotype: 'm'
        },
    },
    {
        name: '海水流速',
        value: 'seawaterVelocity',
        range: {
            min: '11.7',
            max: '31.4',
            step: '0.1',
            logotype: 'm/s'
        },
        rangeTypeOptions: [
            {
                name: '北向速度',
                value: 'northwardSpeed'
            },
            {
                name: '东向流速',
                value: 'eastboundVelocity'
            },
        ]
    },
    {
        name: '风场',
        value: 'windFarms',
        startTime: 1979,
        endTime: 2022,
        range: {
            min: '-10.9',
            max: '7.3',
            step: '0.1',
            logotype: 'm/s'
        },
        rangeTypeOptions: [
            {
                name: '东风',
                value: 'eastWind'
            },
            {
                name: '北风',
                value: 'northWind'
            },
        ]
    },
    {
        name: '大范围降水量',
        value: 'widespreadPrecipitation',
        range: {
            min: '0.0',
            max: '50.0',
            step: '0.1',
            logotype: 'mm'
        },
    },
    {
        name: '对流降水量',
        value: 'convectivePrecipitation',
        range: {
            min: '0.0',
            max: '50.0',
            step: '0.1',
            logotype: 'mm'
        },
    },
    {
        name: '海浪',
        value: 'oceanWave',
        range: {
            min: '0.1',
            max: '2.9',
            step: '0.1',
            logotype: 'm'
        },
    },
    {
        name: '雷暴',
        value: 'thunderstorm',
        range: {
            min: '-42.7',
            max: '35.2',
            step: '0.1',
            logotype: 'k'
        },
    },
    {
        name: '能见度',
        value: 'visibility',
        range: {
            min: '10.8',
            max: '50.7',
            step: '0.1',
            logotype: 'km'
        },
    },
    {
        name: '气温',
        value: 'temperature',
        range: {
            min: '-60.0',
            max: '60.0',
            step: '0.1',
            logotype: '°C'
        },
    },
    {
        name: '总降水量',
        value: 'totalPrecipitation',
    },
    {
        name: '地形地貌',
        value: 'topography',
    },
    {
        name: '热带气旋',
        value: 'tropicalCyclones',
    },
]


const dataTypeConfig = {
    pressure: {
        scalarLayer: {
            colorScale: [
                [193, [90, 86, 143]],
                [206, [72, 104, 181]],
                [219, [69, 151, 168]],
                [233.15, [81, 180, 98]],
                [255.372, [106, 192, 82]],
                [273.15, [177, 209, 67]],
                [275.15, [215, 206, 60]],
                [291, [214, 172, 64]],
                [298, [213, 137, 72]],
                [311, [205, 94, 93]],
                [328, [144, 28, 79]]
            ],
            minValue: 99000,
            maxValue: 105000
        },
    },
    salt: {
        scalarLayer: {
            colorScale: [
                [193, [90, 86, 143]],
                [206, [72, 104, 181]],
                [219, [69, 151, 168]],
                [233.15, [81, 180, 98]],
                [255.372, [106, 192, 82]],
                [273.15, [177, 209, 67]],
                [275.15, [215, 206, 60]],
                [291, [214, 172, 64]],
                [298, [213, 137, 72]],
                [311, [205, 94, 93]],
                [328, [144, 28, 79]]
            ],
            minValue: 20,
            maxValue: 35
        }
    },
    wind: {
        scalarLayer: {
            colorScale: [
                [193, [90, 86, 143]],
                [206, [72, 104, 181]],
                [219, [69, 151, 168]],
                [233.15, [81, 180, 98]],
                [255.372, [106, 192, 82]],
                [273.15, [177, 209, 67]],
                [275.15, [215, 206, 60]],
                [291, [214, 172, 64]],
                [298, [213, 137, 72]],
                [311, [205, 94, 93]],
                [328, [144, 28, 79]]
                //
                // [-11,  [0, 0, 255]],
                // [-1.9, [0, 255, 255]],
                // [0.3,  [255, 255, 0]],
                // [1.8,  [255, 165, 0]],
                // [8,    [255, 0, 0]],
            ],
            minValue: 0.01,
            maxValue:
                8
        },
        vectorLayer: {
            colorScale: [
                // "rgb(222,255,253)",
                // "rgb(234,234,234)",
                // "rgb(255,255,255)",
                // "rgb(156,156,156)",
                "rgb(213,11,11)",
                "rgb(213,11,11)",
                "rgb(213,11,11)",
                "rgb(213,11,11)",
                "rgb(213,11,11)",
            ],
            opacity:
                0.7,
            maxVelocity:
                35,
            lineWidth:
                1,
            particleMultiplier:
                1 / 500,
            frameRate:
                20,
        }
    },
    wave: {
        scalarLayer: {
            colorScale: [
                [193, [90, 86, 143]],
                [206, [72, 104, 181]],
                [219, [69, 151, 168]],
                [233.15, [81, 180, 98]],
                [255.372, [106, 192, 82]],
                [273.15, [177, 209, 67]],
                [275.15, [215, 206, 60]],
                [291, [214, 172, 64]],
                [298, [213, 137, 72]],
                [311, [205, 94, 93]],
                [328, [144, 28, 79]]
            ],
            minValue:
                -10.3,
            maxValue:
                5.9
        }
        ,
        vectorLayer: {
            colorScale: [
                "rgb(222,255,253)",
                "rgb(234,234,234)",
                "rgb(255,255,255)",
                "rgb(156,156,156)",
                "rgb(255,106,43)",
            ],
            opacity:
                0.7,
            maxVelocity:
                10,
            lineWidth:
                6,
            particleMultiplier:
                1 / 500,
            frameRate:
                30,
        }
    },
    current: {
        scalarLayer: {
            colorScale: [
                [193, [90, 86, 143]],
                [206, [72, 104, 181]],
                [219, [69, 151, 168]],
                [233.15, [81, 180, 98]],
                [255.372, [106, 192, 82]],
                [273.15, [177, 209, 67]],
                [275.15, [215, 206, 60]],
                [291, [214, 172, 64]],
                [298, [213, 137, 72]],
                [311, [205, 94, 93]],
                [328, [144, 28, 79]]
            ],
            minValue:
                0.001,
            maxValue:
                2
        }
    }
    ,
    seaTemp: {
        scalarLayer: {
            colorScale: [
                [193, [90, 86, 143]],
                [206, [72, 104, 181]],
                [219, [69, 151, 168]],
                [233.15, [81, 180, 98]],
                [255.372, [106, 192, 82]],
                [273.15, [177, 209, 67]],
                [275.15, [215, 206, 60]],
                [291, [214, 172, 64]],
                [298, [213, 137, 72]],
                [311, [205, 94, 93]],
                [328, [144, 28, 79]]
            ],
            minValue:
                11.7,
            maxValue:
                31.4
        }
    },
    seawaterVelocity: {
        scalarLayer: {
            colorScale: [
                [193, [90, 86, 143]],
                [206, [72, 104, 181]],
                [219, [69, 151, 168]],
                [233.15, [81, 180, 98]],
                [255.372, [106, 192, 82]],
                [273.15, [177, 209, 67]],
                [275.15, [215, 206, 60]],
                [291, [214, 172, 64]],
                [298, [213, 137, 72]],
                [311, [205, 94, 93]],
                [328, [144, 28, 79]]
            ],
            minValue:
                0.1,
            maxValue:
                1.2
        }
    }
    ,
    common: {
        scalarLayer: {
            colorScale: [
                [193, [90, 86, 143]],
                [206, [72, 104, 181]],
                [219, [69, 151, 168]],
                [233.15, [81, 180, 98]],
                [255.372, [106, 192, 82]],
                [273.15, [177, 209, 67]],
                [275.15, [215, 206, 60]],
                [291, [214, 172, 64]],
                [298, [213, 137, 72]],
                [311, [205, 94, 93]],
                [328, [144, 28, 79]]
            ],
            minValue:
                -30.0,
            maxValue:
                40
        }
        ,
        vectorLayer: {
            colorScale: [
                "rgb(222,255,253)",
                "rgb(234,234,234)",
                "rgb(255,255,255)",
                "rgb(156,156,156)",
                "rgb(255,106,43)",
            ],
            opacity:
                0.7,
            maxVelocity:
                5,
            lineWidth:
                1,
            velocityScale:
                0.1,
            particleMultiplier:
                1 / 500,
            frameRate:
                20,
        }
    }
    ,
}


const quarterLine = [
    {
        name: ' 第一季度',
        value: '0',
    },
    {
        name: ' 第二季度',
        value: '1',
    },
    {
        name: ' 第三季度',
        value: '2',
    },
    {
        name: ' 第四季度',
        value: '3',
    },
    {
        name: ' 第五季度',
        value: '4',
    },
]
const monthLine = [
    {
        name: '一月',
        value: '01',
    },
    {
        name: '二月',
        value: '02',
    },
    {
        name: '三月',
        value: '03',
    },
    {
        name: '四月',
        value: '04',
    },
    {
        name: '五月',
        value: '05',
    },
    {
        name: '六月',
        value: '06',
    },
    {
        name: '七月',
        value: '07',
    },
    {
        name: '八月',
        value: '08',
    },
    {
        name: '九月',
        value: '09',
    },
    {
        name: '十月',
        value: '10',
    },
    {
        name: '十一月',
        value: '11',
    },
    {
        name: '十二月',
        value: '12',
    },
]

export {dataTypeOption, dataTypeConfig, quarterLine, monthLine}
