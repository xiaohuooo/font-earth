export default {
    url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    zoom: 4,
    minZoom: 2.3,
    center: [10.06465289, 85.80242172],
    map: '',
}
