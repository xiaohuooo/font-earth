import Vue from 'vue'
import Vuex from 'vuex'
import globalMap from './modules/globalMap.js'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    globalMap,
  },
  getters
})

export default store
