import Vue from 'vue'

const globalMap = {
    state: {
        loading: false
    },
    mutations: {
        SET_LOADING: (state, loading) => {
            state.loading = loading
        }
    },
    actions: {
        initialize(initAll) {
            if (!initAll){
                var zoom = 4;
                var center = [10.06465289, 85.80242172];
                Vue.prototype.$GlobalMap.map.setView(center, zoom);
                Vue.prototype.$GlobalMap.zoom = zoom;
                Vue.prototype.$GlobalMap.center = center;
            }

            Vue.prototype.$GlobalMap.map.eachLayer((layer) => {
                if (layer instanceof L.Path) {
                    layer.remove();
                }
            });

        },
        loading({ commit }, type){
            commit('SET_LOADING', type)
        }
    }
}

export default globalMap
