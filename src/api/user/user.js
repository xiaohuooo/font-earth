import api from '@/common/request';
import { data } from 'jquery';

export function getUser() {
    return api.get('/user/list');
}
export function getPas() {
    return api.get('/user/pwd_reset');
}
export function getForbidden() {
    return api.get('/user/forbidden');
}
export function postCreate(data) {
    return api.post('/user/create',data);
}