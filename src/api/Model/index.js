import api from '@/common/request';

export function getModels() {
    return api.get('/models');
}