import api from '@/common/request';

export function getWindData(params) {
    return api.get('/natural/wind', {
        params: {...params}
    });
}
export function getConditions(params) {
    return api.get('/natural/conditions');
}
