
import api from '@/common/request';

export function getAssess(params) {
    return api.get('/evaluate/model', {
        params: {...params}
    });
}
export function getGeo() {
    return api.get('/geo/country', );
}

export function humanities(params, data) {

    let keys = Object.keys(params);

    var url = '/evaluate/humanities'

    keys.forEach(item => {
        if (url.indexOf("?") != -1) {
            url = url + "&" + item + "=" + params[item]

        } else {
            url = url + "?" + item + "=" + params[item]
        }
    })
    return api.post(url,data);
}
