import api from '@/common/request';
import qs from "qs"
export function getHumanities() {
    const result = api.get('/humanities_classify');
    return result;
}
export function getList(data) {
    const result = api.get('/humanities/list', {
        params: {
            ...data
        }
    });
    return result;
}
export function postList(str, data) {
    const result = api.post('/humanities/modify', data, {
        params: {
            first_cate: str.first_cate,
            second_cate: str.second_cate,
        }
    });
    return result;
}
export function getPie(data) {
    const result = api.get('/humanities/pie', {
        params: {
            ...data
        }
    });
    return result;
}