import api from '@/common/request';
export function getLogin(data) {
    const result = api.get('/user/login' ,{
        params: {
            ...data
        }
    });
    return result;
}