// ? 这个文件，用来封装axios

import axios from 'axios';
import {
  setToken,
  getToken,
  removeToken
} from '@/utils/auth.js'

// 请求头
axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'
// ? 1. 创建axios实例
const instance = axios.create({
  baseURL:process.env.VUE_APP_BASE_API,
  timeout: 100000,
});

// ? 2. 添加请求拦截器
// ? 请求发送之前
instance.interceptors.request.use((config) => {
  // ? 一般而言，是写token的地方
  // //console.log(config)
  const user = getToken('user')
  if (user) {
    config.headers.Authorization = `Bearer ` + user
  }
  return config;
}, (error) => Promise.reject(error));

// ? 3. 添加响应拦截器
// ? 响应回来之后
instance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    return Promise.reject(error)
  },
);

// ? 这个模块默认导出 axios 实例
export default instance;