module.exports = {
    // 设置非根目录
    // publicPath: '/mtview/',
	devServer: {
        proxy: {
            '/api': {
                // 此处的写法，我访问的是http://localhost:8080/api/dataHome.json设置代理后，axios请求不需要把域名带上，只需要把路径前面加上 /api 即可。
                target: 'http://117.73.9.156:8088/api',
                // target: 'http://192.168.1.100:8000/api',
                // 允许跨域
                // 在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
                changeOrigin: true,

                // 是否代理
                ws: false,

                // 路径重置
                // 由于设置了 pathRewrite，所以请求时会把 /api 替换成 ''，最终的请求路径为 /dataHome.json
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    }
 }
